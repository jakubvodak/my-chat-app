//
//  MyTextField.swift
//  ChatApp
//
//  Created by Jakub Vodak on 05/09/2017.
//  Copyright © 2017 Jakub Vodak. All rights reserved.
//

import UIKit

class MyTextField: UITextField {

    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        
        
        textColor = .green
        
        //textColor = UIColor(red: 180/255, green: 180/255, blue: 180/255, alpha: 1)
    }
}
