//
//  Message.swift
//  ChatApp
//
//  Created by Jakub Vodak on 05/09/2017.
//  Copyright © 2017 Jakub Vodak. All rights reserved.
//

import Foundation

class Message {
    
    var text: String
    
    var sender: String
    
    
    init(text: String, sender: String) {
        
        self.text = text
        
        self.sender = sender
    }
    
}


