//
//  ChatViewController.swift
//  ChatApp
//
//  Created by Jakub Vodak on 04/09/2017.
//  Copyright © 2017 Jakub Vodak. All rights reserved.
//

import UIKit
import FirebaseDatabase


protocol Chat {
    
    func ulozVDatabazi(text : String)
}


class ChatViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, Chat {
    
    var name: String?
    
    var messages = [Message]()
    
    
    @IBOutlet var textField: UITextField!
    
    @IBOutlet var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        sledujDatabazi()
    }
    
    @IBAction func sendMessage() {
        
        if let text = textField.text {
            
            if text.characters.count >= 1 {
                
                //Lokalni
                //messages.append(text)
                
                ulozVDatabazi(text: text)
                
                
                textField.text = nil
                
                tableView.reloadData()
                
                return
            }
        }
        
        UIAlertView(title: "Chyba", message: "Zadej text zprávy", delegate: nil, cancelButtonTitle: "Ok").show()
    }
    
    
    func ulozVDatabazi(text : String) {
        
        //Internet
        let json = [
            "name": name,
            "text": text
        ]
        
        // FIREBASE     //FIRDatabase
        let trigger = Database.database().reference(withPath: "Messages").childByAutoId()
        
        
        trigger.setValue(json) { (error, ref) in
            
            guard let error = error else {
                
                return
            }
            
            UIAlertView(title: "Chyba", message: error.localizedDescription, delegate: nil, cancelButtonTitle: "Ok").show()
        }
    }
    
    
    
    
    func sledujDatabazi() {
        
        //FIRDatabase
        Database.database().reference(withPath: "Messages").observe(.value, with: { (snapshot) -> Void in
            
            self.messages.removeAll()
            
            for dictionary in snapshot.children.allObjects {
                
                
                
                                           //FIRDataSnapshot
                if let json = dictionary as? DataSnapshot,
                    let messageJson = json.value as? [String: String],
                    let sender = messageJson["name"],
                    let text = messageJson["text"] {
                    
                 
                    let message = Message(text: text, sender: sender)
                    
                    self.messages.append(message)
                }
            }
            
            self.messages.reverse()
            
            self.tableView.reloadData()
            
        })
    }
    
    
    
    
    // Table View
    
    //Data source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "cell")
        
        let message = messages[indexPath.row]
        
        cell.textLabel?.text = message.text
        
        cell.detailTextLabel?.text = message.sender
        
        return cell
    }
    
    
    //Delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //messages.remove(at: indexPath.row)
        //tableView.reloadData()
    }
    
    
    
    
    @IBAction func gestureFunc() {
        
        ulozVDatabazi(text: "👨‍🏫👩‍🏭👨‍🏭👩‍💻👨‍💻👩‍💼👨‍💼👩‍🔧👨‍🔧")
    }
    
}


