//
//  ViewController.swift
//  ChatApp
//
//  Created by Jakub Vodak on 04/09/2017.
//  Copyright © 2017 Jakub Vodak. All rights reserved.
//

import UIKit

class UserViewController: UIViewController {

    
    @IBOutlet var textField: MyTextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        
        if UserDefaults.standard.bool(forKey: "infoAction") == false {
        
            infoAction()
        }
        
        
        
        
        
       let myCustomView = UIView(frame: CGRect(x: 100, y: 200, width: 200, height: 250))
        
        myCustomView.backgroundColor = .blue
        
        myCustomView.autoresizingMask = .flexibleWidth
        
        view.addSubview(myCustomView)
        
        
        
        
        
        
        
        let notificationCenter = NotificationCenter.default
        
        notificationCenter.addObserver(self, selector: #selector(infoAction), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let chatViewController = segue.destination as? ChatViewController {
            
            chatViewController.name = textField.text
            
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        
        if let text = textField.text {
            
            if text.characters.count >= 2 {
            
                return true
            }
        }
        
        UIAlertView(title: "Chyba", message: "Zadej jméno", delegate: nil, cancelButtonTitle: "Ok").show()
        
        return false
    }
    
    
    @IBAction func infoAction() {
        
        UserDefaults.standard.set(true, forKey: "infoAction")

        UIAlertView(title: "Hello", message: "Pokusna aplikace", delegate: nil, cancelButtonTitle: "Ok").show()
    }
}

